var mongoose = require('mongoose');  
var UserSchema = new mongoose.Schema({  
  mobileNumber: { type: String },
  firstName:  { type: String },
  lastName:  { type: String },
  dateOfBirth:  { type: String },
  gender:  { type: String },
  email:  { type: String },
  confirmPassword: { type: String },
  password:  { type: String }
});
mongoose.model('User', UserSchema);

module.exports = mongoose.model('User');