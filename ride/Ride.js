var mongoose = require('mongoose');  
var RideSchema = new mongoose.Schema({  
    leavingFrom:{ type: String },
    heading:{ type: String },
    going:{ type: String },
    time:{ type: String },
    freeSeat:{ type: String },
    priceForOnePassenger:{ type: String }
});
mongoose.model('Ride', RideSchema);

module.exports = mongoose.model('Ride');