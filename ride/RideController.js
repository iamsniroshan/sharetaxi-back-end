var express = require('express');
var router = express.Router();

var VerifyToken = require(__root + 'auth/VerifyToken');

var User = require('./Ride');

// CREATES A NEW USER WITH MOBILE
router.post('/details', function (req, res) {
    User.create({
            leavingFrom : req.body.leavingFrom,
            heading:req.body.heading,
            going:req.body.going,
            time:req.body.time,
            freeSeat:req.body.freeSeat,
            priceForOnePassenger:req.body.priceForOnePassenger,
        }, 
        function (err, user) {
            if (err) return res.status(500).send("There was a problem adding the information to the database.");
            res.status(200).send(user);
        });
});




module.exports = router;