## ShareTaxi backend - JWT in NodeJS ##

This app includes following APIs:
- **user mobile auth: POST** `http://localhost:3000/api/users/mobile/auth`: This will require data in POST request

  ###### Body part:
  ```
    mobileNumber:    0779456432

  ```

- **user registration : POST** `http://localhost:3000/api/users/create`: This will require data in POST request

  ###### Body part:
  ```
    firstName:Niroshan
    lastName:Selvaraja
    gender:Male
    dateOfBirth:1989.03.21
    email:iamsniroshan@gmail.com
    password:Abc@123
    confirmPassword: Abc@123
    user_id: 616c64421d6a9516803c680c // This id you have to take it from above response
 
  ```
- **Ride details update** `http://localhost:3000/api/ride/details`: This will require data in POST request

  ###### Body part:
  ```
    leavingFrom:leavingFrom
    heading:heading
    going:going
    time:10.30pm
    freeSeat: 5
    priceForOnePassenger: 1200
    user_id: 616c64421d6a9516803c680c // This id you have to take it from above response
 
  ```

- **How to run** 
    ```
    npm run start
 
    ```