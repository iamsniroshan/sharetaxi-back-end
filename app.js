var express = require('express');
var app = express();
var db = require('./db');
app.use(express.json())
app.use(express.urlencoded({extended: true}))

global.__root   = __dirname + '/'; 

app.get('/api', function (req, res) {
  res.status(200).send('API works.');
});

var UserController = require(__root + 'user/UserController');
app.use('/api/users', UserController);

var rideController = require(__root + 'ride/RideController');
app.use('/api/ride', rideController);

var AuthController = require(__root + 'auth/AuthController');
app.use('/api/auth', AuthController);



module.exports = app;